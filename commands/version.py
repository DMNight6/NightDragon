@bot.command()
async def version(ctx):
    await ctx.message.delete()
    embed = discord.Embed(title="Some Module Info", color=discord.Color.from_rgb(255, 215, 0))
    embed.add_field(name="discord.py", value=f"{discord.__version__}")
    embed.add_field(name="Release Version", value=f"{discord.version_info}")
    embed.add_field(name="hata", value=f"{hata.version}")
    await ctx.send(embed=embed)