import discord, praw, lavalink, re, hata
import os, sys, sqlite3, json, psutil, requests, subprocess
import time, datetime, logging
import math, random, asyncio, itertools, functools

from discord.ext import commands, menus
from discord.utils import get
from discord import FFmpegPCMAudio, utils
from async_timeout import timeout
from pathlib import Path
from datetime import date
from os import system
from sys import platform
from dotenv import load_dotenv

# Above this is all module used

logger = logging.getLogger(__name__)
handler = logging.FileHandler('./Logs/Bot/blog.log', "a")
handler.setLevel(logging.DEBUG)
handlerFormat = logging.Formatter('%(asctime)s:[%(levelname)s] - %(message)s')
handler.setStream(sys.stdout)
handler.setFormatter(handlerFormat)

load_dotenv() # Loading .env for window OS / Ubuntu distro

pre = sqlite3.connect(database="System/Prefixes.db") # SQLite3 connection (Local)
cre = pre.cursor() # This is where stuff get SELECT, DELETE, INSERT

token = os.environ.get("TOKEN") # Fetching Bot Token Inside ENV file
guild_id = 413632902480396298 # Certain stuff not to remember :)

exec(open("./System/function.py", "r").read()) # Defined function :)

bot = commands.AutoShardedBot(
    command_prefix=get_prefix, 
    case_insensitive=True, 
    status=discord.Status.idle, 
    activity=discord.Activity(type=2, name="System Mainframe")
    )

bot.remove_command('help')

for events in os.listdir('events'):
    if events.endswith('.py'):
        with open(f"events/{events}") as rk1:
            exec(rk1.read()) # Events

for command in os.listdir('commands'):
    if command.endswith('.py'):
        with open(f'commands/{command}') as rk:
            exec(rk.read()) # Command

bot.run(token) # Start bot.