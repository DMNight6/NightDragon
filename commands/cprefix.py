@bot.command(description="Changes Bot Prefix||<prefix>")
@commands.has_permissions(administrator=True) # Check if user have the permission
async def cprefix(ctx, prefix=None):
    await ctx.message.delete()
    member = discord.Member
    if prefix == None or prefix == member.mention: # Check if prefix is a mention or empty
        await commands.CommandInvokeError("Prefix cannot be empty or mentions!") # Returns as a error
    else:
        try:
            cre.execute("UPDATE sprefix SET prefix = $1 WHERE guildid = $2",(prefix, ctx.guild.id)) # This tries to change the prefix, error is handled from try: to except:
            pre.commit()
            await ctx.send(f"Changed Prefix To {prefix}")
        except: # Whenever a error occured while changing, the following tells you it failed
            await commands.CommandInvokeError(f"Failed To Change Prefix To {prefix}") # [Handled in error_handler.py]