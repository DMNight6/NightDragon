@bot.command(description="(Same function as clear but with specific ids)", aliases=["clearid"])
async def purgeid(ctx, amount: int): # Personal use for clearing messages
    if owner_id_identify:
        await ctx.channel.purge(limit=amount + 1)
    else:
        await ctx.send(f"You have no access")